# Front-end-engineering
> 工程化，规范化

## 脚手架

- angularjs-quick-start
- vue-quick-start
- react-quick-start
- use-react-create-app
- yue-react-open

## 构建工具

- webpack
- hello-webapck
- fis3 `打包angular 1.x出错，待修正`

#### 参考资料

- [fis 打包后的文件是/，要开服务才能正常，为什么不用./呢](https://github.com/fex-team/fis3/issues/990)
- [fis3-hook-relative](https://github.com/fex-team/fis3-hook-relative)

## 编译

- babel 

#### 参考资料
- [try it out](http://babeljs.io/repl/)
- [babel-handbook / Babel 用户手册](https://github.com/jamiebuilds/babel-handbook/blob/master/translations/zh-Hans/user-handbook.md)
 
## 测试

- eslint 
- json-server

## 持续集成

- travis-ci 








